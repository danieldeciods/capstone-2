const User = require("../models/user");
const Course = require("../models/course");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const express = require("express");

//Check if email already exists
module.exports.emailExists = (params) => {
  return User.find({ email: params.email }).then((result) => {
    return result.length > 0 ? true : false;
  });
};

//Registration
module.exports.register = (params) => {
  let newUser = new User({
    firstname: params.firstname,
    lastname: params.lastname,
    email: params.email,
    mobileNo: params.mobileNo,
    password: bcrypt.hashSync(params.password, 10),
  });
  return newUser.save().then((user, err) => (err ? false : true));
};

//login
module.exports.login = (params) => {
  return User.findOne({ email: params.email }).then((result) => {
    if (result == null) {
      return false;
    }
    const isPasswordMatched = bcrypt.compareSync(
      params.password,
      result.password
    );
    if (isPasswordMatched) {
      return { access: auth.createAccessToken(result.toObject()) };
    } else {
      return false;
    }
  });
};

//get user profile
module.exports.get = (params) => {
  return User.findById(params.userId).then(result => {
    result.password = undefined
    return result
  })
}

module.exports.getUser = (params) => {
  return User.findById(params.userId).then((result) => {
    result.password = undefined
    return result;
  });
};

//enroll to a class
module.exports.enroll = (params) => {
  return User.findById(params.userId)
    .then((resultUser) => {
      resultUser.enrollments.push({ courseId: params.courseId });
      return resultUser.save().then((resultSave, err) => {
        return Course.findById(params.courseId).then((resultCourse) => {
          resultCourse.enrollees.push({ userId: params.userId });
          return resultCourse.save().then((resultSaveCourse, err) => {
            return err ? false : true;
          });
        });
      });
    })
    .catch((err) => { });
};
