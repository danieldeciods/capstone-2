const Course = require("../models/course");

//View allcourses
module.exports.getAll = () => {
  return Course.find({}).then((result) => result);
};


//View all active courses
module.exports.getAllActive = () => {
  return Course.find({ isActive: true }).then((result) => result);
};

//Create/Register Course
module.exports.add = (params) => {
  let newCourse = new Course({
    name: params.name,
    description: params.description,
    price: params.price,
  });

  return newCourse.save().then((course, err) => {
    return err ? false : true;
  });
};

//Get a course
module.exports.getCourse = (params) => {
  return Course.findById(params.courseId).then((result) => result);
};

//View All Courses
module.exports.getAll = () => {
  return Course.find({}).then((result) => result);
};

//Modify Course
module.exports.update = (params) => {
  let updatedCourse = {
    name: params.name,
    description: params.description,
    price: params.price,
  };

  return Course.findByIdAndUpdate(params.courseId, updatedCourse).then(
    (course, err) => {
      return err ? false : true;
    }
  );
};

//Archive
module.exports.archive = (params) => {
  let updateActive = {
    isActive: false,
  };

  return Course.findByIdAndUpdate(params.courseId, updateActive).then(
    (course, err) => {
      return err ? false : true;
    }
  );
};

//Activate
module.exports.activate = (params) => {
  let updateActive = {
    isActive: true
  };

  return Course.findByIdAndUpdate(params.courseId, updateActive).then((course, err) => {
    return err ? false : true
  })
}
