const mongoose = require("mongoose");
const Schema = require("mongoose");
const model = require("mongoose");

const userSchema = new mongoose.Schema({
  firstname: {
    type: String,
    required: [true, "First name is rquired"],
  },
  lastname: {
    type: String,
    required: [true, "Last name is required"],
  },
  email: {
    type: String,
    required: [true, "Email is required"],
  },
  password: {
    type: String,
    required: [true, "Password is required"],
  },
  mobileNo: {
    type: String,
    required: [true, "Mobile Number is required"],
  },
  isAdmin: {
    type: Boolean,
    default: false,
  },
  enrollments: [
    {
      courseId: {
        type: String,
        required: [true, "Course ID .is required"],
      },
      enrolledOn: {
        type: Date,
        default: new Date(),
      },
      status: {
        type: String,
        default: "Enrolled",
      },
    },
  ],
});

module.exports = mongoose.model("User", userSchema);
