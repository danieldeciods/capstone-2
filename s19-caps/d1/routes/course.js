const express = require("express");
const auth = require("../auth");
const CourseController = require("../controllers/courseController");
const course = require("../models/course");
const router = express.Router();

//getallactive
router.get("/active", (req, res) => {
  CourseController.getAllActive().then((result) => res.send(result));
});

//getallcourses
router.get("/", (req, res) => {
  CourseController.getAll().then((result) => res.send(result));
});

//get single course
router.get("/:courseId", (req, res) => {
  let courseId = req.params.courseId;
  CourseController.getCourse({ courseId }).then((result) => res.send(result));
});

//create new cours
router.post("/", auth.verify, (req, res) => {
  CourseController.add(req.body).then((result) => res.send(result));
});

//update course
router.put("/", auth.verify, (req, res) => {
  CourseController.update(req.body).then((result) => res.send(result));
});
//archive a course
router.delete("/:courseId", auth.verify, (req, res) => {
  let courseId = req.params.courseId;
  CourseController.archive({ courseId }).then((result) => res.send(result));
});

//activate course
router.patch("/:courseId/activate", auth.verify, (req, res) => {
  let courseId = req.params.courseId;
  CourseController.activate({ courseId }).then((result) => res.send(result));
});

module.exports = router;
