const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes = require("./routes/user");
const courseRoutes = require("./routes/course");
//database connection
const connect = () => {
  return mongoose.connect(
    "mongodb+srv://admin:ifqQ3IcNQTjCcwZu@learnmongo-9fdce.gcp.mongodb.net/restbooking?retryWrites=true&w=majority",
    {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false
    }
  );
};
//Server Setup
const app = express();
const port = 3000;
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());
app.use("/api/users", userRoutes);
app.use("/api/courses", courseRoutes);

connect()
  .then(async (connection) => {
    app.listen(port, () => {
      console.log(`Server is up at port ${port}`);
    });
  })
  .catch((e) => console.error(e));
