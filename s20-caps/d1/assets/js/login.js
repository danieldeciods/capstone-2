let loginForm = document.querySelector("#logInUser")

loginForm.addEventListener("submit", (e) => {
	e.preventDefault()

	let email = document.querySelector("#userEmail").value
	let password = document.querySelector("#password").value

	if(email =="" || password ==""){
		alert("Please input your email and/or password")
	} else {
		fetch('http://localhost:3000/api/users/login', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => {
			return res.json()
		})
		.then(data => {
				if(data !== false && data.access !== null){
				localStorage.setItem('token', data.access)

				fetch('http:localhost:3000/api/users/details', {
					headers: {
						Authorization: `Bearer ${data.access}`
					}
				})
				.then(res => {
					return res.json()
				})
				.then(data => {

					localStorage.setItem("id", data._id)
					localStorage.setItem("isAdmin", data.isAdmin)
					
					const adminUser = localStorage.getItem("isAdmin")
					if(adminUser == "false" || !adminUser){
						window.location.replace("./profile.html")
					} else {
						window.location.replace("./courses.html")
					}



				})
			} else {
				alert("The Username or Password is incorrect")
			}
		})
	}
})
