let adminUser = localStorage.getItem("isAdmin")
let modalButton = document.querySelector("#adminButton")
let students = document.querySelector("#students")
let cardFooter; 

//1d----------------------------------
if (adminUser == "false" || !adminUser) {
	profileNav.innerHTML = 
	`
		<a href="./profile.html" class="nav-link" id="profileNav"> Profile </a>
	`
} else {
	profileNav.innerHTML =
	`
	<li class="nav-item ">
		<a href="./profile.html" class="nav-link" id="profileNav" style="display:none;"> Profile </a>
	</li>
	`
}

//-------------------------------------
if (adminUser == "false" || !adminUser) {
	modalButton.innerHTML = null;
	 
} else {
	modalButton.innerHTML = 
		`
			<div class="col-md-2 offset-md-10">
				<a href="./addCourse.html" class="btn btn-block btn-primary">
					Add Course
				</a>
			</div>
		`
}

if(adminUser == "false" || !adminUser){
	fetch('http:localhost:3000/api/courses/active')
	.then(res => res.json())
	.then(data => {
		let courseData;
		let viewCourse;

		if(data.length < 1){
			courseData = "No courses available"
		} else {
			courseData = data.map(course => {
				
				cardFooter = `<a href="./course.html?courseId=${course._id}" value=${course._id} class="btn btn-primary text-white btn-block">Go to Course</a>`
											
				return (
					`
					<div class="col-md-6 my-3">
						<div class="card">
							<div class="card-body">
								<h5 class="card-title">${course.name}</h5>
								<p class="card-text text-left">${course.description}</p>
								<p class="card-text text-right">${course.price}</p>
							</div>
							<div class="card-footer">
								${cardFooter}
							</div>
						</div>
					</div>
					`
				)
			}).join("")
		}

	let container = document.querySelector("#coursesContainer")
	
	container.innerHTML = courseData

	})

} else {
	fetch('http://localhost:3000/api/courses/')
	.then(res => res.json())
	.then(data => {
		let courseData;
		let viewCourse;

		if(data.length < 1){
			courseData = "No courses available"
		} else {
			courseData = data.map(course => {
				
				if(`${course.isActive}` == "false"){

					cardFooter = `<a href="./activateCourse.html?courseId=${course._id}" value=${course._id} class="btn btn-success text-white btn-block"> Activate Course </a>`
					viewCourse = `<a href="./course.html?courseId=${course._id}" value=${course._id} class="btn btn-secondary text-white btn-block">View Course</a>`
				} else {
					
					cardFooter = `<a href="./deleteCourse.html?courseId=${course._id}" value=${course._id} class="btn btn-danger text-white btn-block"> Archive Course </a>`
					viewCourse = `<a href="./course.html?courseId=${course._id}" value=${course._id} class="btn btn-secondary text-white btn-block">View Course</a>`
				}
			
				return (
					`
						<div class="col-md-6 my-3">
							<div class="card">
								<div class="card-body">
									<h5 class="card-title">${course.name}</h5>
									<p class="card-text text-left">${course.description}</p>
									<p class="card-text text-right">${course.price}</p>
									<p class="card-text text-center">${course.price}</p>
								</div>
								<div class="card-footer">
									${cardFooter}
									${viewCourse}
								</div>
							</div>
						</div>
					`
				)
			}).join("")
		}

	let container = document.querySelector("#coursesContainer")
	
	container.innerHTML = courseData

	})
}