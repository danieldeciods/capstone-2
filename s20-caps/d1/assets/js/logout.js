//clear storage
localStorage.clear();

if (adminUser == "false" || !adminUser) {
	navbar.innerHTML = 
	`
		<a href="./profile.html" class="nav-link" id="profileNav"> Profile </a>
	`
} else {
	profileNav.innerHTML =
	`
	<li class="nav-item ">
		<a href="./profile.html" class="nav-link" id="profileNav" style="display:none;"> Profile </a>
	</li>
	`
}


//redirect to login page
window.location.replace('./../../pages/login.html);