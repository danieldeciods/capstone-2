  
 
let token = localStorage.getItem("token")

let userName = document.querySelector("#userName")
let userEmail = document.querySelector("#userEmail")
let mobileNo = document.querySelector("#mobileNo")
let enrollments = document.querySelector("#enrollments")

fetch("http://localhost:3000/api/users/details", {
	method: "GET",
	headers: {
 		"Content-Type": "application/json",
 		"Authorization": `Bearer ${token}`
 	} 
})			
.then(res => {return res.json()})
.then(data => {
	userName.innerHTML = data.firstname + " " + data.lastname
	userEmail.innerHTML = data.email
	mobileNo.innerHTML = data.mobileNo
	let courseName = data.enrollments
	console.log(data.enrollments)

	
	courseName.map(course => {
		fetch(`http://localhost:3000/api/courses/${course.courseId}`)
		.then(res => res.json())
		.then(data => {
			courseData = data
			courseData =
				(
				`<div class="col-md-6 my-3 mx-auto">
					<div class="card">
						<div class="card-body text-center">
							<h5 class="card-title">${courseData.name}</h5>
						</div>
					</div>
				</div>`
				)
			container.innerHTML += courseData
		})
		let container = document.querySelector("#coursesContainer")
	})
})	

