
let params = new URLSearchParams(window.location.search)
let courseId = params.get("courseId")
let token = localStorage.getItem("token")

if (adminUser == "false" || !adminUser) {
	navbar.innerHTML = 
	`
		<a href="./profile.html" class="nav-link" id="profileNav"> Profile </a>
	`
} else {
	profileNav.innerHTML =
	`
	<li class="nav-item ">
		<a href="./profile.html" class="nav-link" id="profileNav" style="display:none;"> Profile </a>
	</li>
	`
}


fetch(`http://localhost:3000/api/courses/${courseId}`, {
	method: "DELETE",
	headers: {
 		"Authorization": `Bearer ${token}`
 	},
}).then(res => {return res.json()
}).then(data => {
 	if(data){
 		alert("You have deleted the course successfully")
 		window.location.replace("./courses.html")
 	} else {
		alert("Deleting failed")
	}
})
	
