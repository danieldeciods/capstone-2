let registerForm = document.querySelector("#registerUser")

if (adminUser == "false" || !adminUser) {
	navbar.innerHTML = 
	`
		<a href="./profile.html" class="nav-link" id="profileNav"> Profile </a>
	`
} else {
	profileNav.innerHTML =
	`
	<li class="nav-item ">
		<a href="./profile.html" class="nav-link" id="profileNav" style="display:none;"> Profile </a>
	</li>
	`
}


registerForm.addEventListener("submit", (e) => {
	e.preventDefault()

	let firstName = document.querySelector("#firstName").value
	let lastName = document.querySelector("#lastName").value
	let mobileNo = document.querySelector("#mobileNumber").value
	let email = document.querySelector("#userEmail").value
	let password1 = document.querySelector("#password1").value
	let password2 = document.querySelector("#password2").value

	if((password1 !== "" && password2 !== "") && (password1 === password2) && (mobileNo.length == 11)){
		fetch("http://localhost:3000/api/users/email-exists", {
			method: "POST",
			headers: {
				"Content-Type" : "application/json"
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(response => response.json())
		.then(data => {
			if(data === false){
				fetch("http://localhost:3000/api/users", {
					method: "POST",
					headers: {
						"Content-Type" : "application/json"
					},
					body: JSON.stringify({
						firstname: firstName,
						lastname: lastName,
						email: email,
						password: password1,
						mobileNo: mobileNo
					})
				})
				.then(response => response.json())
				.then(data => {
					if (data){
						alert("Registration successful")
						window.location.replace("./pages/profile.html")
					} else {
						alert("Registration went wrong")
					}
				})
			} else {
				alert("Email already exists")
			}
		})
	} else {
		alert("Invalid inputs")
	}
})

